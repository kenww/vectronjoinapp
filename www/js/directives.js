angular.module('starter.directives', [])

.directive('emailExists', ['$http', 'API_URL', function($http, API_URL){
    return {
        require : 'ngModel',
        link : function($scope, element, attrs, ngModel) {
            //ngModel.$asyncValidators.usernameAvailable = function(username) {
            ngModel.$asyncValidators.emailExists = function(email) {
                //console.log(email);

                return $http.get(API_URL+'/emailCheck?email='+email);


            };
        }
    }
}]);