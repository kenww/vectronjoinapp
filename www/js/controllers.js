angular.module('starter.controllers', [])

.controller('AppCtrl', function($scope, $rootScope, $ionicModal, $timeout, AuthService, $ionicLoading) {



  // Form data for the login modal
  $scope.loginData = {};

  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope,
    animation: 'slide-in-up',
    backdropClickToClose: false,
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };

  $rootScope.logout = function(){
    $rootScope.$broadcast('logout');
  };


  //listening logout event
  $rootScope.$on('logout', function(e, d){
    AuthService.logoutService();
    $scope.modal.show();
    console.log('logout');
  });

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);

    if(!$scope.loginData.email || !$scope.loginData.password){
      return;
    }
    $ionicLoading.show().then(function(){
      AuthService.loginService($scope.loginData).then(function(e){
        $timeout(function() {
          $ionicLoading.hide().then(function(){
            $scope.closeLogin();
            $scope.loginData = {};
            $rootScope.$broadcast('loadSubscriberVenues');
          });
        }, 1000);
      }, function(){
        $ionicLoading.hide().then(function(){
        });
      });
    });
  };
})

.controller('NewMemberCtrl', function($scope, $rootScope, $ionicLoading, $localstorage, $ionicModal, $timeout, Members, AuthService, Venues, $q, md5) {

  $scope.member={};
  //subscriber lists
  $scope.venues = {};

  // $scope.alerts = [];
  // $scope.success = [];

  $scope.removeAlert = function(index){
    $rootScope.alerts.splice(index, 1);
  };
  $scope.removeSuccess = function(index){
    $rootScope.success.splice(index, 1);
  };

  var loadSubscriberVenues = function(){
    Venues.getVenues().then(function(s){
      console.log(s);
      $scope.venues = s;
      // if($rootScope.subscriberLists.length>0){
      //   $scope.member.subscriber_lists_id = $rootScope.subscriberLists[0].id+'';
      // }
    },function(e){
      console.log(e);
    });
  };

  $rootScope.$on('loadSubscriberVenues', function(e, d){
    loadSubscriberVenues();
  });

  $scope.addMember = function(member, venue_id, operator, form){
    if(!venue_id || !operator){
        $rootScope.alerts.push({
          msg: 'Please choose the venue and select operator.'
        });
        return;
    }
    // console.log(member);
    member.mobile = member.mobile_string.replace(new RegExp(" ", 'g'), '');
    $ionicLoading.show().then(function(){
      Members.addMember(member, venue_id, operator).then(function(s){
        console.log(s);
        $timeout(function() {
          $ionicLoading.hide().then(function(){
            $scope.member={};
            form.$setPristine();
            $rootScope.success.push({
              msg: 'Thanks for your joining'
            });
            $timeout(function(){
              $rootScope.success = [];
            }, 3000);
          });
        }, 1000);

      },function(e){
        console.log(e);
        $timeout(function() {
          $ionicLoading.hide().then(function(){
            $rootScope.alerts.push({
              msg: e.data.message
            });
          });
        }, 1000);
      });
    });
  };



  // $rootScope.$on('logout', function(e, d){
  //   $scope.member={};
  //   $rootScope.lists = {};
  // });


  //login modal

  $scope.showLogin = function(){
    // Form data for the login modal
    $scope.loginData = {};
    $scope.step = 0;
    $scope.error = '';
    $scope.forceLogin = false;

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
    }).then(function(modal) {
      $scope.modal = modal;
      $scope.modal.show();
    });


    //close modal
    $scope.closeLogin = function(step) {
      $scope.modal.hide();
    };


    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);

      if(!$scope.loginData.username || !$scope.loginData.password){
        return;
      }

      $ionicLoading.show().then(function(){
        AuthService.loginService($scope.loginData, $rootScope.device).then(function(e){
          $rootScope.$broadcast('loadSubscriberVenues');
          $timeout(function() {
            $ionicLoading.hide().then(function(){
              // $scope.closeLogin();
              $scope.loginData = {};
              $scope.step = 1;
              $scope.error = null;
            });
          }, 1000);
        }, function(e){
          $ionicLoading.hide().then(function(){
            $scope.error = e.data.message;
          });
        });


        // $timeout(function() {
        //   $ionicLoading.hide().then(function(){
        //     $scope.step = 1;
        //     // $rootScope.$broadcast('loadSubscriberLists');
        //   });
        // }, 1000);
      });
    };

    //use password to access setting
    $scope.secretLogin = function(){
      if(md5.createHash($scope.loginData.password+'http://www.vectron.com.au/') === AuthService.secret()){
        $rootScope.$broadcast('loadSubscriberVenues');
        $timeout(function() {
          $ionicLoading.hide().then(function(){
            // $scope.closeLogin();
            $scope.error = null;
            $scope.loginData = {};
            $scope.step = 1;
          });
        }, 1000);
      }
      else{
        // console.log('incorrect password');
        $scope.error = 'incorrect password';
      }
    };


    //save device current settings
    $scope.doSetting =function(){
      $localstorage.set('OPERATOR', $scope.loginData.operator);
      $localstorage.set('VENUE_ID', $scope.loginData.venue_id);
      $rootScope.operator = $localstorage.get('OPERATOR');
      $rootScope.venue_id = parseInt($localstorage.get('VENUE_ID'));
      $rootScope.getCurrentVenue($rootScope.venue_id);
      $scope.modal.hide();
    };

    $scope.doForceLogin = function(){
      $scope.forceLogin = true;
    }

  };

  $rootScope.logout = function(){
    $rootScope.$broadcast('logout');
  };


  //listening logout event
  $rootScope.$on('logout', function(e, d){
    if(AuthService.accessToken()){
      AuthService.logoutService($rootScope.device).then(function(s){
        console.log('s');
        $rootScope.operator = null;
        $rootScope.venue_id = null;
        $rootScope.currentVenue = {};
        $scope.closeLogin();
      }, function(){
        console.log('e');
        $rootScope.operator = null;
        $rootScope.venue_id = null;
        $rootScope.currentVenue = {};
        $scope.closeLogin();
      });
    }
    else{
      console.log('s');
      $rootScope.operator = '';
      $rootScope.venue_id = '';
    }
  });


  // $scope.operators = [
  //   {operator:'Ken'},
  //   {operator:'Will'},
  //   {operator:'Blair'}
  // ];

  // $scope.callbackMethod = function (query, isInitializing) {
  //   var d = $q.defer();
  //   d.resolve($scope.operators);
  //   return d.promise;
  // }

})

.controller('PlaylistCtrl', function($scope, $stateParams) {
});
